from django import forms
from .models import *

# FORMULARIOS PRINCIPALES

class DomicilioForm(forms.ModelForm):
    class Meta:
        model = Domicilio
        fields= '__all__'
        widgets = {
            
            'fecha': forms.DateInput(attrs={'class':'form-control datepicker'}),
            'direccion': forms.TextInput(attrs={'class':'form-control'}),
            'telefono': forms.TextInput(attrs={'class':'form-control'}),
            'cliente': forms.Select(attrs={ 'id':'cliente', 'style':"width: 100%"}),
            'tecnico' : forms.Select(attrs={'id':'tecnico', 'style':"width: 100%"}),
            'servicio' : forms.Select(attrs={'class':'selectpicker'}),
            'estado' : forms.Select(attrs={'class':'selectpicker'}),
            'valor': forms.NumberInput(attrs={'step': 0.25}),
            
            
         }

class EmpleadoForm(forms.ModelForm):
    class Meta:
        model = Empleado
        fields= '__all__'
        widgets = {
            
            'identificacion': forms.TextInput(attrs={'class':'form-control'}),
            'nombres': forms.TextInput(attrs={'class':'form-control'}),
            'apellidos': forms.TextInput(attrs={'class':'form-control'}),
            'fecha_nac': forms.DateInput(attrs={'class':'form-control datepicker'}),
            'direccion': forms.TextInput(attrs={'class':'form-control'}),
            'lugar_nac': forms.TextInput(attrs={'class':'form-control'}),
            'contacto': forms.TextInput(attrs={'class':'form-control'}),
            'email': forms.TextInput(attrs={'class':'form-control'}),
            'observacion': forms.Textarea(attrs={'class':'form-control'}),
            

            'tipo' : forms.Select(attrs={'class':'selectpicker'}),
            'cod_cargo' : forms.Select(attrs={'class':'selectpicker'}),
            'eps' : forms.Select(attrs={'class':'selectpicker' }),
            'arl' : forms.Select(attrs={'class':'selectpicker'}),
            'pension' : forms.Select(attrs={'class':'selectpicker'}),
            'cesantias' : forms.Select(attrs={'class':'selectpicker'}),
            
         }


class ClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields= '__all__'
        widgets = {
            
            'ide': forms.TextInput(attrs={'class':'form-control form-control-sm'}),
            'nombres': forms.TextInput(attrs={'class':'form-control form-control-sm','placeholder':'.form-control-sm'}),
            'apellidos': forms.TextInput(attrs={'class':'form-control form-control-lg','placeholder':'.form-control-lg'}),
            'direccion': forms.TextInput(attrs={'class':'form-control form-control-sm'}),
            'contacto': forms.TextInput(attrs={'class':'form-control'}),
            'email': forms.TextInput(attrs={'class':'form-control'}),
            'otro_telf': forms.TextInput(attrs={'class':'form-control'}),
            'observacion': forms.Textarea(attrs={'class':'form-control'}),
            

            'tipo' : forms.Select(attrs={'class':'selectpicker'}),
            'user' : forms.Select(attrs={'class':'selectpicker'}),
            'tipo_persona' : forms.Select(attrs={'class':'selectpicker'}),
            
            
         }

class ProductoForm(forms.ModelForm):
    class Meta:
        model = Producto
        fields= '__all__'
        widgets = {
            
            'referencia': forms.TextInput(attrs={'class':'form-control'}),
            'descripcion': forms.TextInput(attrs={'class':'form-control'}),
            'costo': forms.NumberInput(attrs={'class':'form-control'}),
            'precio_unit': forms.NumberInput(attrs={'class':'form-control'}),
            'stock_minimo': forms.NumberInput(attrs={'class':'form-control'}),
            'observacion': forms.Textarea(attrs={'class':'form-control'}),
            
            'familia' : forms.Select(attrs={'class':'selectpicker'}),
            'estante' : forms.Select(attrs={'class':'selectpicker'}),
            
         }

class ProveedorForm(forms.ModelForm):
    class Meta:
        model = Proveedor
        fields= '__all__'
        widgets = {
            
            'ide': forms.TextInput(attrs={'class':'form-control'}),
            'razon_social': forms.TextInput(attrs={'class':'form-control'}),
            'direccion': forms.TextInput(attrs={'class':'form-control'}),
            'contacto': forms.TextInput(attrs={'class':'form-control'}),
            'email': forms.TextInput(attrs={'class':'form-control'}),
            'observacion': forms.Textarea(attrs={'class':'form-control'}),
            

            'tipo' : forms.Select(attrs={'class':'selectpicker'}),
            'tipo_persona' : forms.Select(attrs={'class':'selectpicker'}),
            
            
         }

# FORMULARIOS SECUNDARIOS

class CargoForm(forms.ModelForm):
    class Meta:
        model = Cargo
        fields= '__all__'
        widgets = {
            'ide': forms.NumberInput(attrs={'class':'form-control'}),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
        
         }

class EstadoForm(forms.ModelForm):
    class Meta:
        model = Estado
        fields= '__all__'
        widgets = {
            'ide': forms.NumberInput(attrs={'class':'form-control'}),
            'detalle': forms.TextInput(attrs={'class':'form-control'}),
        
         }

class EstanteForm(forms.ModelForm):
    class Meta:
        model = Estante
        fields= '__all__'
        widgets = {
            'ide': forms.NumberInput(attrs={'class':'form-control'}),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
        
         }

class FamiliaForm(forms.ModelForm):
    class Meta:
        model = Familia
        fields= '__all__'
        widgets = {
            'ide': forms.NumberInput(attrs={'class':'form-control'}),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
        
         }

class SeguridadSocialForm(forms.ModelForm):
    class Meta:
        model = SeguridadSocial
        fields= '__all__'
        widgets = {
            'ide': forms.NumberInput(attrs={'class':'form-control'}),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'tipo' : forms.Select(attrs={'class':'selectpicker'}),
         }

class TipoIdentificacionForm(forms.ModelForm):
    class Meta:
        model = TipoIdentificacion
        fields= '__all__'
        widgets = {
            'ide': forms.TextInput(attrs={'class':'form-control'}),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
        
         }

class TipoPersonaForm(forms.ModelForm):
    class Meta:
        model = TipoPersona
        fields= '__all__'
        widgets = {
            'ide': forms.TextInput(attrs={'class':'form-control'}),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
        
         }

class TipoSeguridadForm(forms.ModelForm):
    class Meta:
        model = TipoSeguridad
        fields= '__all__'
        widgets = {
            'ide': forms.NumberInput(attrs={'class':'form-control'}),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
        
         }


#### VENTAS

class VentaForm(forms.ModelForm):
    class Meta:
        model = Orden
        fields= '__all__'
        widgets = {
            
            'codigo': forms.TextInput(attrs={'class':'form-control', }),
            'cliente': forms.Select(attrs={ 'id':'cliente', 'style':"width: 100%"}),
            'is_ordered': forms.TextInput(attrs={'class':'form-control'}),
            'items': forms.Select(attrs={'class':'selectpicker'}),
            'date_ordered': forms.TextInput(attrs={'class':'form-control'}),
 
         }

class OrdenItemForm(forms.ModelForm):
    class Meta:
        model = OrdenItem
        fields= '__all__'
        widgets = {
            
            'producto': forms.Select(attrs={ 'id':'cliente', 'style':"width: 100%"}),
            'is_ordered': forms.TextInput(attrs={'class':'form-control'}),
            'date_added': forms.Select(attrs={'class':'selectpicker'}),
            'date_ordered': forms.TextInput(attrs={'class':'form-control'}),
 
         }