from django.urls import path,include
from AppFerreteria import views

urlpatterns = [
    #URLS DOMICILIOS
    path('CrearDomicilio/', views.DomicilioCreate.as_view(), name='crear_domicilio' ),
    path('ListaDomicilio/', views.listaDomicilios, name='lista_domicilios' ),

    #URLS EMPLEADOS
    path('CrearEmpleado/', views.EmpleadoCreate.as_view(), name='crear_empleado' ),
    path('ListaEmpleados/', views.listaEmpleados, name='lista_empleados' ),
    path('ModificarEmpleado/<int:pk>/', views.EmpleadoUpdate.as_view(), name='modificar_empleado' ),
    path('EliminarEmpleado/<int:pk>/', views.EmpleadoDelete.as_view(), name='eliminar_empleado' ),
    
    #URLS CLIENTES
    path('CrearCliente/', views.ClienteCreate.as_view(), name='crear_cliente' ),
    path('ListaClientes/', views.listaClientes, name='lista_clientes' ),
    path('ModificarCliente/<int:pk>/', views.ClienteUpdate.as_view(), name='modificar_cliente' ),
    path('EliminarCliente/<int:pk>/', views.ClienteDelete.as_view(), name='eliminar_cliente' ),

    #URLS PRODUCTOS
    path('CrearProducto/', views.ProductoCreate.as_view(), name='crear_producto' ),
    path('ListaProductos/', views.listaProductos, name='lista_productos' ),
    path('ModificarProducto/<int:pk>/', views.ProductoUpdate.as_view(), name='modificar_producto' ),
    path('EliminarProducto/<int:pk>/', views.ProductoDelete.as_view(), name='eliminar_producto' ),

    #URLS PROVEEDORES
    path('CrearProveedor/', views.ProveedorCreate.as_view(), name='crear_proveedor' ),
    path('ListaProveedores/', views.listaProveedores, name='lista_proveedores' ),
    path('ModificarProveedor/<int:pk>/', views.ProveedorUpdate.as_view(), name='modificar_proveedor' ),
    path('EliminarProveedor/<int:pk>/', views.ProveedorDelete.as_view(), name='eliminar_proveedor' ),

    #URLS VENTA
    path('CrearFactura/', views.CrearVenta.as_view(), name='crear_venta' ),
    path('AgregarProductoLista/', views.CrearOrdenItem.as_view(), name='agregar_producto_lista' ),
    path('listaProductosAj/', views.ListaAjaxProductos.as_view(), name='lista_productosaj' ),
    
    



# MINICRUDS

    #URLS CARGOS
    path('Crearcargo/', views.CargoCreate.as_view(), name='crear_cargo' ),
    path('Listacargos/', views.listaCargo, name='lista_cargos' ),
    path('Modificarcargo/<int:pk>/', views.CargoUpdate.as_view(), name='modificar_cargo' ),
    path('Eliminarcargo/<int:pk>/', views.CargoDelete.as_view(), name='eliminar_cargo' ),

    #URLS ESTADO
    path('CrearEstado/', views.EstadoCreate.as_view(), name='crear_Estado' ),
    path('ListaEstados/', views.listaEstado, name='lista_Estados' ),
    path('ModificarEstado/<int:pk>/', views.EstadoUpdate.as_view(), name='modificar_Estado' ),
    path('EliminarEstado/<int:pk>/', views.EstadoDelete.as_view(), name='eliminar_Estado' ),

    #URLS ESTANTE
    path('CrearEstante/', views.EstanteCreate.as_view(), name='crear_Estante' ),
    path('ListaEstantes/', views.listaEstante, name='lista_Estantes' ),
    path('ModificarEstante/<int:pk>/', views.EstanteUpdate.as_view(), name='modificar_Estante' ),
    path('EliminarEstante/<int:pk>/', views.EstanteDelete.as_view(), name='eliminar_Estante' ),

    #URLS FAMILIA
    path('CrearFamilia/', views.FamiliaCreate.as_view(), name='crear_Familia' ),
    path('ListaFamilias/', views.listaFamilia, name='lista_Familias' ),
    path('ModificarFamilia/<int:pk>/', views.FamiliaUpdate.as_view(), name='modificar_Familia' ),
    path('EliminarFamilia/<int:pk>/', views.FamiliaDelete.as_view(), name='eliminar_Familia' ),

    #URLS SEGURIDAD SOCIAL
    path('CrearSeguridadSocial/', views.SeguridadSocialCreate.as_view(), name='crear_SeguridadSocial' ),
    path('ListaSeguridadSocial/', views.listaSeguridadSocial, name='lista_SeguridadSocial' ),
    path('ModificarSeguridadSocial/<int:pk>/', views.SeguridadSocialUpdate.as_view(), name='modificar_SeguridadSocial' ),
    path('EliminarSeguridadSocial/<int:pk>/', views.SeguridadSocialDelete.as_view(), name='eliminar_SeguridadSocial' ),
    
    #URLS TIPO IDENTIFICACION
    path('CrearTipoIdentificacion/', views.TipoIdentificacionCreate.as_view(), name='crear_TipoIdentificacion' ),
    path('ListaTipoIdentificacion/', views.listaTipoIdentificacion, name='lista_TipoIdentificacion' ),
    path('ModificarTipoIdentificacion/<slug:pk>/', views.TipoIdentificacionUpdate.as_view(), name='modificar_TipoIdentificacion' ),
    path('EliminarTipoIdentificacion/<slug:pk>/', views.TipoIdentificacionDelete.as_view(), name='eliminar_TipoIdentificacion' ),

    #URLS TIPO PERSONA
    path('CrearTipoPersona/', views.TipoPersonaCreate.as_view(), name='crear_TipoPersona' ),
    path('ListaTipoPersona/', views.listaTipoPersona, name='lista_TipoPersona' ),
    path('ModificarTipoPersona/<slug:pk>/', views.TipoPersonaUpdate.as_view(), name='modificar_TipoPersona' ),
    path('EliminarTipoPersona/<slug:pk>/', views.TipoPersonaDelete.as_view(), name='eliminar_TipoPersona' ),

    #URLS TIPO SEGURIDAD
    path('CrearTipoSeguridad/', views.TipoSeguridadCreate.as_view(), name='crear_TipoSeguridad' ),
    path('ListaTipoSeguridad/', views.listaTipoSeguridad, name='lista_TipoSeguridad' ),
    path('ModificarTipoSeguridad/<int:pk>/', views.TipoSeguridadUpdate.as_view(), name='modificar_TipoSeguridad' ),
    path('EliminarTipoSeguridad/<int:pk>/', views.TipoSeguridadDelete.as_view(), name='eliminar_TipoSeguridad' ),


]