
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_delete
from django.conf import settings


class Cargo(models.Model):
    nombre = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.nombre


class Cliente(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, blank=True, null=True)
    tipo = models.ForeignKey( 'TipoIdentificacion', on_delete=models.PROTECT)
    nombres = models.CharField(max_length=100, blank=True, null=True)
    apellidos = models.CharField(max_length=100, blank=True, null=True)
    contacto = models.CharField(max_length=100, blank=True, null=True)
    otro_telf = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    tipo_persona = models.ForeignKey( 'TipoPersona', on_delete=models.PROTECT)
    direccion = models.CharField(max_length=200, blank=True, null=True)
    observacion = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.nombres + " " + self.apellidos + " | cc. " + self.ide


class Empleado(models.Model):
    identificacion = models.CharField(max_length=50, primary_key=True)
    tipo = models.ForeignKey( 'TipoIdentificacion',on_delete=models.PROTECT)
    nombres = models.CharField(max_length=100, blank=True, null=True)
    apellidos = models.CharField(max_length=100, blank=True, null=True)
    cod_cargo = models.ForeignKey( 'Cargo', on_delete=models.PROTECT)
    fecha_nac = models.CharField(max_length=100,blank=True, null=True)
    lugar_nac = models.CharField(max_length=300, blank=True, null=True)
    direccion = models.CharField(max_length=200, blank=True, null=True)
    contacto = models.CharField(max_length=100, blank=True, null=True)
    fotografia = models.ImageField(upload_to = 'ImgEmpleados/', blank=True, null=True,)
    email = models.CharField(max_length=100, blank=True, null=True)
    observacion = models.TextField(blank=True, null=True)
    eps = models.ForeignKey( 'SeguridadSocial',on_delete=models.PROTECT, related_name='fk_eps' )
    arl = models.ForeignKey( 'SeguridadSocial',on_delete=models.PROTECT, related_name='fk_ark')
    pension = models.ForeignKey( 'SeguridadSocial',on_delete=models.PROTECT, related_name='fk_pension' )
    cesantias = models.ForeignKey( 'SeguridadSocial',on_delete=models.PROTECT, related_name='fk_cesantias')

    def __str__(self):
        return self.identificacion + " "+ self.nombres + " " + self.apellidos


class Estado(models.Model):
    detalle = models.CharField(max_length=50, blank=True, null=True)
       
    def __str__(self):
        return self.id

class Estante(models.Model):
    nombre = models.CharField(max_length=200, blank=True, null=True)
        
    def __str__(self):
        return self.nombre

class Familia(models.Model):
    nombre = models.CharField(max_length=200, blank=True, null=True)
        
    def __str__(self):
        return self.nombre

class Producto(models.Model):
    referencias = models.CharField( max_length=50)
    descripcion = models.CharField(max_length=200, blank=True, null=True)
    fotografia = models.ImageField(upload_to = 'ImgProductos/', blank=True, null=True)
    costo = models.FloatField(blank=True, null=True)
    precio_unit = models.FloatField(blank=True, null=True)
    stock_minimo = models.IntegerField(blank=True, null=True)
    familia = models.ForeignKey( Familia,on_delete=models.PROTECT)
    estante = models.ForeignKey( Estante, on_delete=models.PROTECT)
    observacion = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.referencias 

class OrdenItem(models.Model):
    producto = models.OneToOneField(Producto, on_delete=models.PROTECT)

    def __str__(self):
        return self.producto.descripcion

class Orden(models.Model):
    codigo= models.CharField(max_length=15)
    cliente = models.ForeignKey(Cliente, on_delete=models.PROTECT)
    is_ordered = models.BooleanField(default=False)
    items = models.ManyToManyField(OrdenItem)
    date_ordered = models.DateTimeField(auto_now=True)

    def get_cart_items(self):
        return self.items.all()
    
    def get_cart_total(self):
        return sum([item.producto.precio_unit for item in self.items.all()])

    def __str__(self):
        return '{0} - {1}'.format(self.owner, self.ref_code)

class Proveedor(models.Model):
    tipo = models.ForeignKey( 'TipoIdentificacion',on_delete=models.PROTECT)
    razon_social = models.CharField(max_length=500, blank=True, null=True)
    contacto = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    tipo_persona = models.ForeignKey( 'TipoPersona', on_delete=models.PROTECT)
    direccion = models.CharField(max_length=200, blank=True, null=True)
    observacion = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.id

class TipoSeguridad(models.Model):
    nombre = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.nombre

class SeguridadSocial(models.Model):
    nombre = models.CharField(max_length=200, blank=True, null=True)
    tipo = models.ForeignKey( 'TipoSeguridad',on_delete=models.PROTECT)

    def __str__(self):
        return self.nombre

class TipoIdentificacion(models.Model):
    ide = models.CharField( max_length=4)
    nombre = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.nombre

class TipoPersona(models.Model):
    nombre = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.nombre


class Servicio(models.Model):
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre

class EstadoDomicilio(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre


class Domicilio(models.Model):
    fecha = models.DateTimeField()
    direccion = models.CharField(max_length=200)
    cliente = models.ForeignKey('Cliente', on_delete=models.PROTECT)
    telefono = models.CharField(max_length=50)
    servicio = models.ForeignKey('Servicio', on_delete=models.PROTECT)
    tecnico = models.ForeignKey('Empleado', on_delete=models.PROTECT)
    valor = models.DecimalField(max_digits=16, decimal_places=2)
    estado = models.ForeignKey('EstadoDomicilio', on_delete=models.PROTECT)
    
    def __str__(self):
        return self.direccion


@receiver(post_delete, sender=Empleado)
def fotografia_delete(sender, instance, **kwargs):
    """ Borra los ficheros de las fotos que se eliminan. """
    instance.fotografia.delete(False)

