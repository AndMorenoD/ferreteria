from rest_framework.generics import ListAPIView
from AppFerreteria.models import Producto
from .serializers import ProductosSerializer

class ApiListaProductos(ListAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductosSerializer