from django.apps import AppConfig


class AppferreteriaConfig(AppConfig):
    name = 'AppFerreteria'
