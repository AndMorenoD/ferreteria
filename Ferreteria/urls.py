"""Ferreteria URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from Ferreteria import views

urlpatterns = [
    
    path('', views.bienvenida, name='bienvenida'),
    path('GestionSoftware', views.gestionSoftware, name='gestionSoftware'),
    path('Base', views.base, name='base'),
    path('api-auth/', include('rest_framework.urls')),
    path('Ferreteria/',include('AppFerreteria.urls')),
    path('Api/',include('AppFerreteria.api.urls')),
    path('Usuarios/', include('django.contrib.auth.urls')),
    
    path('admin/', admin.site.urls),
    
]
